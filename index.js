//Declarando as contantes que receberão dos modulos necessários para o software.


const express = require('express'); // carregando o modulo express
const app = express(); // recebendo a função express do modulo express, criando uma cópia do framework
const handlebars = require('express-handlebars');// recebendo na constante handlebars o modulo express hanflebars
const bodyParser = require('body-parser');// recebendo na constante handlebars o modulo body-parser
const Listacontato = require('./models/conexao_listacontatos'); //recebendo a constante recebendo a constante models/conexao_listacontatos


// Configurando para utilizar o handlebars 
app.engine('handlebars',handlebars({defaultLayout: 'main'}))
app.set('view engine','handlebars') // Usando o template handlebars

//Configuração para utilizar o BODY PARSER
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

//ROTAS INICIO  PRINCIPAL RENDERIZANDO PARA O /HOME /views/home.handlebars


app.get('/',function(req,res){
    Listacontato.findAll().then(function(posts){
        console.log(posts)
        res.render('home',{posts: posts})
    })
})




/////////////////////////////////////////ROTA PARA INSERÇÃO
app.get("/cadastro", function(req,res){
    res.render('formulario');// arquivo que será renderizadp
    //res.send("Ola, Bem Vindo"); // Enviar uma mensagem 
});

app.post("/enviar_formulario",function(req,res){ // ROTA CRIADA PARA O METHODO POST e INSERT para o banco
    Listacontato.create({
        nome:  req.body.nome,
        sobrenome: req.body.sobrenome,
        email: req.body.email,
        telefone: req.body.telefone,

    }).then(function()  { //REDIRECIONAR PAGINA QUANDO FUNCIONA O POST 
      //  res.send("Sucesso")
      res.redirect('/');
    }).catch((err) => {
        res.send("Falha"+erro)
    });
})   
/////////////////////////////////////////ROTA PARA EDIÇÃO

app.get('/editar/:id', (req, res) => {
    id = req.params.id;
    res.render('editar')
})

app.post('/editar', (req, res) => {
    Listacontato.update({
        nome:  req.body.nome,
        sobrenome: req.body.sobrenome,
        email: req.body.email,
        telefone: req.body.telefone
    }, {
        where: { id: id },
    }).then(() => {
        res.redirect('/')
    })
})

//////////////ROTA EXCLUIR

app.get('/deletar/:id',function(req,res){
    Listacontato.destroy({where: {'id':req.params.id}}).then(function(){
    res.redirect('/');
    res.send('Postagem excluida com sucesso!')
    
    }).catch(function(erro){
        res.send('Postagem não existe!')
    })
    })




app.listen(3000, function(){
    console.log("Servidor rodando na porta 3000, acesse localhost:3000");
    }); // DEVE SER A ULTIMA Para criar a conexão do servidor no express. 