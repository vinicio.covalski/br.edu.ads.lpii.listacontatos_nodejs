const Sequelize = require('sequelize'); // Na pasta models criando Sequelize com constante, esse dado não será alterado durante o projeto.
const sequelize = new Sequelize('listacontatos','root','123456', {

    host: "localhost", // Servidor
    dialect: 'mysql'   // Define qual SGBD será conectado

});//Criando o construtor que vai receber as informações da conexão do banco.


module.exports = {

    Sequelize: Sequelize, 
    sequelize: sequelize

    //O sistema de módulos do Node.js é responsável por criar o objeto module.export e o export aponta para esse objeto, podendo ser usado para retornar funções e objetos bastando somente adicioná-los ao export, ou seja reaproveitamento do código
}