const banco = require('./db_listacontatos')  // Criando o objeto como constante que recebe as informações para conexão do banco.

const db_operacoes = banco.sequelize.define('contato',{
//Função de criação da tabela no banco, contato, definir o nome do atributo exemplo nome : { type: objetobanco.Sequelize.STRING},


        nome:{
            type: banco.Sequelize.STRING
             },
        sobrenome:{
            type: banco.Sequelize.STRING
                  },
          email: {
              type: banco.Sequelize.STRING
                 },
        telefone:{
            type: banco.Sequelize.STRING
                 }

})

//db_operacoes.sync({force:true}) // Quando utilizado, ele força a criação da tabela no banco, dropando a tabela se existe, o ideal manter comentado.

module.exports = db_operacoes; // Exportando para reaproveitar a constante db_operacoes